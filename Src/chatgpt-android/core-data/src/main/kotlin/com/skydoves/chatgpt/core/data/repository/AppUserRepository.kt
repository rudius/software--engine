package com.skydoves.chatgpt.core.data.repository


import com.skydoves.chatgpt.core.model.network.ChatResponse
import com.skydoves.chatgpt.core.model.network.CreateBotResponse
import com.skydoves.chatgpt.core.model.network.LoginResponse
import com.skydoves.chatgpt.core.model.network.SigninResponse
import kotlinx.coroutines.flow.Flow

interface AppUserRepository {
  abstract val uidFlow: Flow<String?>

  suspend fun login(username: String, password: String): LoginResponse?

  suspend fun signin(username: String, password: String): SigninResponse?

  suspend fun chat(username: String, botId: String,question:String,conversationId:String): ChatResponse?

  suspend fun createBot(username: String, botName: String, botDescription:String, prompt:String):CreateBotResponse?

}
