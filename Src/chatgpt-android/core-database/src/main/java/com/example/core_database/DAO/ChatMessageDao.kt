package com.example.core_database.DAO

import com.example.core_database.entities.ChatMessage
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ChatMessageDao {
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(chatMessage: ChatMessage)

  @Query("SELECT * FROM chat_messages WHERE botId = :botId ORDER BY timestamp ASC")
  fun getMessagesForUserAndBot( botId: String): Flow<List<ChatMessage>>

  @Delete
  fun deleteMessage(message: ChatMessage)
}
