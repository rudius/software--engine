package com.example.core_database.DAO

import com.example.core_database.entities.Bot
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface BotDao {
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(bot: Bot)

  @Query("SELECT * FROM bots ORDER BY lastMessageTime DESC")
  fun getAllBots(): Flow<List<Bot>>


  @Delete
  fun deleteBot(bot: Bot)

  @Query("SELECT * FROM bots WHERE botId = :botId")
  fun getBotByBotId(botId: String): Bot

  @Update
  fun updateBot(bot: Bot)
}



