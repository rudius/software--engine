package com.example.core_database.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Entity(
  tableName = "bots",
  foreignKeys = [
    ForeignKey(
      entity = User::class,
      parentColumns = ["userId"],
      childColumns = ["userId"],
      onDelete = ForeignKey.CASCADE
    )
  ],
  indices = [Index(value = ["userId"])] // 为userId创建索引
)


data class Bot(
  @PrimaryKey val botId: String,
  val userId: String, // 外键，关联用户
  val conversationId:String,
  val botName: String,
  val description: String,
  val settings: String, // 可以存储其他配置项
  val head:String,
  var lastMessageTime:Long,
  var lastMessageContent: String
)
