package com.example.core_database.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
  tableName = "chat_messages",
  foreignKeys = [
    ForeignKey(entity = User::class, parentColumns = ["userId"], childColumns = ["userId"]),
    ForeignKey(entity = Bot::class, parentColumns = ["botId"], childColumns = ["botId"],onDelete = ForeignKey.CASCADE)

  ],
  indices = [Index(value = ["userId"]), Index(value = ["botId"])] // 为userId和botId创建索引

)
data class ChatMessage(
  @PrimaryKey(autoGenerate = true) val messageId: Int=0,
  val userId: String,
  val botId: String,
  val messageContent: String,
  val timestamp: Long,
  val isSentByUser: Boolean // 表示消息是用户发出还是机器人回复
)
