package com.example.core_database.DAO


import com.example.core_database.entities.User
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface UserDao {
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(user: User)

  @Query("SELECT * FROM users WHERE userId = :userId")
  fun getUserById(userId: String): User?

  @Update
  fun updateuser(user: User)
}
