package com.example.core_database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
  @PrimaryKey val userId: String,
  val userNickname: String,
  val preference: String,
  val head:String
)
