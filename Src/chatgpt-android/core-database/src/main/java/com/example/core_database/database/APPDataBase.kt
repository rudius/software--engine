package com.example.core_database.database


import com.example.core_database.entities.User
import com.example.core_database.DAO.UserDao
import com.example.core_database.entities.Bot
import com.example.core_database.DAO.BotDao
import com.example.core_database.entities.ChatMessage
import com.example.core_database.DAO.ChatMessageDao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = [User::class, Bot::class, ChatMessage::class], version = 1, exportSchema = true)
abstract class AppDatabase : RoomDatabase() {
  abstract fun userDao(): UserDao
  abstract fun botDao(): BotDao
  abstract fun chatMessageDao(): ChatMessageDao

  companion object {
    @Volatile
    private var INSTANCE: AppDatabase? = null

    fun getDatabase(context: Context): AppDatabase {
      return INSTANCE ?: synchronized(this) {
        val instance = Room.databaseBuilder(
          context.applicationContext,
          AppDatabase::class.java,
          "chat_gpt_database"
        ).allowMainThreadQueries()
          .build()

        INSTANCE = instance
        instance
      }
    }
  }
}
