/*
 * Designed and developed by 2024 skydoves (Jaewoong Eum)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.chatgpt.navigation


import android.util.Log
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.ddd.coze.feature.userlogin.UserLogin
import com.ddd.coze.feature.usersignin.UserSignin
import com.skydoves.chatgpt.R
import com.skydoves.chatgpt.core.designsystem.component.ChatGPTSmallBottomBar1
import com.skydoves.chatgpt.core.designsystem.component.ChatGPTSmallBottomBar4
import com.skydoves.chatgpt.core.designsystem.component.ChatGPTSmallTopBar
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens
import com.skydoves.chatgpt.feature.chat.ddd.bots.BotsChannels
import com.skydoves.chatgpt.feature.chat.ddd.messages.BotMessagesScreen
import com.skydoves.chatgpt.feature.chat.ddd.Channel4.ChatGPTMiddleBar4
import com.skydoves.chatgpt.core.designsystem.component.ChatGPTSmallBottomBar2
import com.skydoves.chatgpt.core.designsystem.component.ChatGPTSmallBottomBar3
import com.skydoves.chatgpt.feature.chat.ddd.Channel2.CharGPTMiddleBar2
import com.skydoves.chatgpt.feature.chat.ddd.Channel3.ChatGPTMiddleBar3
import com.skydoves.chatgpt.feature.chat.ddd.Channel4.Userchannel


fun NavGraphBuilder.chatGPTHomeNavigation(
  composeNavigator: AppComposeNavigator
) {

  composable(route = ChatGPTScreens.UserLogin.name) {
    UserLogin(composeNavigator = composeNavigator)
  }

  composable(route = ChatGPTScreens.UserSignin.name) {
    UserSignin(composeNavigator = composeNavigator)
  }

  //主页
  composable(route = ChatGPTScreens.Channel1.name) {
    Scaffold(
      topBar = {
        ChatGPTSmallTopBar(
          title = "如聊"
        )
      },
      bottomBar = {
        ChatGPTSmallBottomBar1(composeNavigator = composeNavigator)
      }
    ) { padding ->
      BotsChannels(
        modifier = Modifier
          .padding(padding) ,// 这里会应用顶部的 padding
        composeNavigator = composeNavigator
      )
    }
  }

  //发现：预制bot
  composable(route = ChatGPTScreens.Channel2.name) {
    Scaffold(
      topBar = {
        ChatGPTSmallTopBar(
          title = "如聊"
        )
      },
      bottomBar = {
        ChatGPTSmallBottomBar2(composeNavigator = composeNavigator)
      }
    ) { padding ->
      CharGPTMiddleBar2(
        modifier = Modifier.padding(padding),
        composeNavigator = composeNavigator
      )
    }
  }

  //创建
  composable(route = ChatGPTScreens.Channel3.name) {
    Scaffold(
      topBar = {
        ChatGPTSmallTopBar(
          title = "如聊"
        )
      },
      bottomBar = {
        ChatGPTSmallBottomBar3(composeNavigator = composeNavigator)
      }
    ) { padding ->
      ChatGPTMiddleBar3(
        modifier = Modifier.padding(padding),
        composeNavigator = composeNavigator
      )
    }
  }

  //我的
  composable(route = ChatGPTScreens.Channel4.name) {
    Scaffold(
      topBar = {
        ChatGPTSmallTopBar(
          title = "如聊"
        )
      },
      bottomBar = {
        ChatGPTSmallBottomBar4(composeNavigator = composeNavigator)
      }
    ) { padding ->
      ChatGPTMiddleBar4(
        modifier = Modifier.padding(padding),
        composeNavigator = composeNavigator
      )
    }
  }

  composable(route = ChatGPTScreens.Userchannel.name) {
    Scaffold(
      topBar = {
        ChatGPTSmallTopBar(
          title = "如聊"
        )
      },
      bottomBar = {
        ChatGPTSmallBottomBar4(composeNavigator = composeNavigator)
      }
    ) { padding ->
      Userchannel(
        modifier = Modifier.padding(padding),
        composeNavigator = composeNavigator
      )
    }
  }

  composable(route = ChatGPTScreens.Channels.name) {
    Scaffold(
      topBar = {
        ChatGPTSmallTopBar(
          title = stringResource(id = R.string.app_name)
        )
      },
    ) { padding ->
      BotsChannels(
        modifier = Modifier
          .padding(padding) ,// 这里会应用顶部的 padding
        composeNavigator = composeNavigator
      )
    }
  }

  composable(
    route = ChatGPTScreens.Messages.name,
    arguments = ChatGPTScreens.Messages.navArguments
  ) {
    val botId = it.arguments?.getString("botId") ?: return@composable
    val botName = it.arguments?.getString("botName") ?: return@composable
    val conversationId = it.arguments?.getString("conversationId") ?: return@composable
    Log.i("BotInWith",it.arguments.toString())

    BotMessagesScreen(
      botId = botId,
      botName = botName,
      conversationId=conversationId,
      composeNavigator = composeNavigator,
    )


  }
}
