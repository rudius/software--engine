package com.skydoves.chatgpt.feature.chat.ddd.users
import com.example.core_database.entities.User
import com.example.core_database.DAO.UserDao

class UserRepository(private val userDao: UserDao) {

  // 插入一个用户
  fun insert(user: User) {
    userDao.insert(user)
  }

  // 根据 userId 获取用户
  fun getUserById(userId: String): User? {
    return userDao.getUserById(userId)
  }

  fun updateUser(user: User)
  {
    userDao.updateuser(user)
  }
}
