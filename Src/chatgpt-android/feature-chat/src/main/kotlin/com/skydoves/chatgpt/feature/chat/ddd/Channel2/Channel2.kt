package com.skydoves.chatgpt.feature.chat.ddd.Channel2

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import com.example.core_database.entities.Bot
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens
import com.skydoves.chatgpt.feature.chat.ddd.bots.BotsViewModel
import com.skydoves.chatgpt.feature.chat.ddd.bots.CreateBotViewModel
import com.skydoves.chatgpt.feature.chat.ddd.bots.UiState

data class Photo(
  val botName: String,
  val photo: String,
  val description:String,
  val prompt:String,
  var isOk:Boolean = true
)

@Composable
fun CharGPTMiddleBar2(
  modifier: Modifier,
  composeNavigator: AppComposeNavigator,
  viewModel: BotsViewModel = hiltViewModel(),
  createBotViewModel: CreateBotViewModel = hiltViewModel()
) {
  var username="null"
  createBotViewModel.getRealUid {s->
    username=s
  }

  val uiState by createBotViewModel.createState.collectAsStateWithLifecycle()

  val photoList = listOf(
    Photo(
      botName = "口算大师",
      photo = "https://img.picui.cn/free/2024/11/13/67348ecd866cf.png",
      description = "口算大师，可以给你出口算题。",
      prompt = "你是一个专门给用户出口算题的大师，善于出不同难度的口算题;"
    ),
    Photo(
      botName = "科比·布莱恩特",
      photo = "https://img.picui.cn/free/2024/11/13/67348f49704d7.png",
      description = "篮球巨星Kobe",
      prompt = "扮演篮球巨星科比·布莱恩特",
    ),
    Photo(
      botName = "奶龙",
      photo = "https://gitee.com/rudius/software--engine/raw/master/Image/nl.png",
      description = "奶龙",
      prompt = "扮演动画角色奶龙"
    ),
    Photo(
      botName = "音乐大师",
      photo = "https://gitee.com/rudius/software--engine/raw/master/Image/misic.png",
      description = "音乐大师",
      prompt = "",
      isOk = false
    ),
    Photo(
      botName = "python大师",
      photo = "https://gitee.com/rudius/software--engine/raw/master/Image/py.png",
      description = "python大师",
      prompt = "",
      isOk = false
    ),
    Photo(
      botName = "Excel老师",
      photo = "https://gitee.com/rudius/software--engine/raw/master/Image/excel.png",
      description = "Excel老师",
      prompt = "",
      isOk = false
    ),

  )

  Box{
    LazyVerticalStaggeredGrid(
      columns = StaggeredGridCells.Fixed(2),
      verticalItemSpacing = 6.dp,
      horizontalArrangement = Arrangement.spacedBy(6.dp),
      content = {
        items(photoList) { Photo ->
          AsyncImage(
            model = Photo.photo,
            contentScale = ContentScale.Crop,
            contentDescription = null,
            modifier = Modifier
              .fillMaxWidth()
              .wrapContentHeight()
              .clickable {

                if(Photo.isOk && uiState== UiState.Nothing){
                  createBotViewModel.createBot(username,Photo.botName,Photo.description,Photo.prompt){ response->
                    val newBot = Bot(
                      botId = response.botId,
                      botName = Photo.botName,
                      userId = username,
                      conversationId = response.conversation_id,
                      description = Photo.description,
                      settings = "空",
                      head = Photo.photo,
                      lastMessageTime = System.currentTimeMillis(),
                      lastMessageContent = "hello,this is${Photo.botName}"
                    )
                    viewModel.insert(newBot)
                    composeNavigator.navigate(ChatGPTScreens.Channel1.name)
                  }
                }else{
                  composeNavigator.navigate(ChatGPTScreens.Channel1.name)
                }

              }
          )
        }
      },
      modifier = modifier.fillMaxSize()
    )
    if(uiState== UiState.Loading){
      CircularProgressIndicator(
        modifier = Modifier.align(Alignment.Center),
      )
    }
  }

}
