package com.skydoves.chatgpt.feature.chat.ddd.Channel4

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.example.core_database.entities.User
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens

@Composable
fun Userchannel(
  modifier: Modifier,
  composeNavigator: AppComposeNavigator,
  viewModel: Channel4ViewModel = hiltViewModel(),
) {
  val user by viewModel.user.observeAsState()
  var userNickname by remember { mutableStateOf("") }
  var preference by remember { mutableStateOf("") }

  val image = remember { listOf(
    "https://img.picui.cn/free/2024/11/13/6734870a78ecf.jpg",
    "https://img.picui.cn/free/2024/11/13/67348710b86fd.jpg",
    "https://img.picui.cn/free/2024/11/13/67348710c7cd9.jpg",
    "https://img.picui.cn/free/2024/11/13/67348710d31b5.jpg",
    "https://img.picui.cn/free/2024/11/13/67348710c6ca6.jpg",
    "https://img.picui.cn/free/2024/11/13/67348710c63a7.jpg",
  ) }

  var currentImageIndex by remember { mutableStateOf(0) }

  val rainbowColorsBrush = remember {
    Brush.sweepGradient(
      listOf(
        Color(0xFF9575CD),
        Color(0xFFBA68C8),
        Color(0xFFE57373),
        Color(0xFFFFB74D),
        Color(0xFFFFF176),
        Color(0xFFAED581),
        Color(0xFF4DD0E1),
        Color(0xFF9575CD)
      )
    )
  }
  val borderWidth = 4.dp
  Column(
    modifier = modifier
      .statusBarsPadding()
      .verticalScroll(rememberScrollState()),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center
  ) {
    Spacer(modifier = Modifier.height(20.dp))
    Box(
      modifier = Modifier
        .size(150.dp)
        .padding(8.dp)
        .clip(CircleShape)
    ){
      AsyncImage(
        model = image[currentImageIndex],
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
          .size(150.dp)
          .border(
            BorderStroke(borderWidth, rainbowColorsBrush),
            CircleShape
          )
          .padding(borderWidth)
          .clip(CircleShape)
          .clickable {
            currentImageIndex = (currentImageIndex + 1) % image.size
          }
      )
    }

    OutlinedTextField(
      value = userNickname,
      onValueChange = { userNickname = it },
      label = { Text("用户昵称") },
      maxLines = 1,
      shape = RoundedCornerShape(20.dp,20.dp,20.dp,20.dp),
      modifier = Modifier
        .padding(top = 20.dp)
        .padding(bottom = 20.dp)
        .padding(horizontal = 20.dp)
        .fillMaxWidth()
    )

    OutlinedTextField(
      value = preference,
      onValueChange = { preference = it }, 
      label = { Text("个人简介") },
      maxLines = 3,
      shape = RoundedCornerShape(20.dp,20.dp,20.dp,20.dp),
      modifier = Modifier
        .padding(bottom = 20.dp)
        .padding(horizontal = 20.dp)
        .height(100.dp)
        .fillMaxWidth()
    )

    Button(
      onClick = {
        user?.let {
          User(
            userId = it.userId,
            userNickname = if(userNickname=="") "未命名" else userNickname,
            preference = if(preference=="") "这个人很唐，什么也没有留下" else preference,
            head = image[currentImageIndex],
          )
        }?.let { viewModel.updateUser(it) }
        composeNavigator.navigate(ChatGPTScreens.Channel4.name)
      },
      colors = ButtonDefaults.buttonColors(
        containerColor = Color(0xFF2196F3), // 设置按钮背景颜色
        contentColor = Color.White // 设置文字颜色
      ),
      modifier = Modifier
        .padding(horizontal = 20.dp)
        .padding(top = 100.dp)
        .fillMaxWidth()
    ) {
      Text("登记个人信息")
    }
  }
}