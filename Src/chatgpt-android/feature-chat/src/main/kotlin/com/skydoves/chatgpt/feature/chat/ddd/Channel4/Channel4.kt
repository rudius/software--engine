package com.skydoves.chatgpt.feature.chat.ddd.Channel4

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens
import kotlinx.coroutines.delay

@Composable
fun ChatGPTMiddleBar4(
  modifier: Modifier,
  composeNavigator: AppComposeNavigator,
  viewModel: Channel4ViewModel = hiltViewModel(),
) {

  val user by viewModel.user.observeAsState()

  val image = remember { listOf(
    "https://gitee.com/rudius/software--engine/raw/master/Image/t1.png",
    "https://gitee.com/rudius/software--engine/raw/master/Image/t2.png",
    "https://gitee.com/rudius/software--engine/raw/master/Image/t3.png",
    "https://gitee.com/rudius/software--engine/raw/master/Image/t4.png",
  ) }
  var currentIndex by remember { mutableStateOf(0) }
  LaunchedEffect(Unit) {
    while (true) {
      delay(3500)
      currentIndex = (currentIndex + 1) % image.size
    }
  }
  Column(
    modifier = modifier
      .statusBarsPadding()
      .verticalScroll(rememberScrollState()),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center
  ) {
    Box(
      modifier = Modifier
        .height(600.dp)
        .fillMaxWidth()
    ){
      Crossfade(
        targetState = image[currentIndex],
        animationSpec = tween(durationMillis = 500, delayMillis = 0, easing = FastOutSlowInEasing),
        label = ""
      ) { targetImage ->
        AsyncImage(
          model = targetImage,
          contentDescription = null, // 可以为null
          contentScale = ContentScale.FillBounds,
          modifier = Modifier.fillMaxHeight(0.7f),
          alignment = Alignment.TopStart
        )
      }
      Box(
        modifier = Modifier
          .offset(x = 5.dp, y = 325.dp)
          .size(150.dp)
          .clip(CircleShape)
      ) {
        AsyncImage(
          model = user?.head,
          contentDescription = null,
          contentScale = ContentScale.Crop,
          modifier = Modifier
            .border(4.dp, Color.Black, CircleShape)
            .size(150.dp)
            .clip(CircleShape)
            .clickable {
              composeNavigator.navigate(ChatGPTScreens.Userchannel.name)
            }
        )
      }
      user?.let {
        Text(
          modifier=modifier
            .offset(x = 165.dp, y = 370.dp),
          text= it.userNickname,
          style = MaterialTheme.typography.titleLarge
        )
        Text(
          modifier = Modifier
            .offset(x = 25.dp, y= 500.dp)
            .width(350.dp),
          text = it.preference,
          style = MaterialTheme.typography.titleLarge
        )
      }
    }
  }
}