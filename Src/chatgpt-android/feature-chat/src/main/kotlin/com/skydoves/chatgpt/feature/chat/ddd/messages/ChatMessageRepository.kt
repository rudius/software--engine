package com.skydoves.chatgpt.feature.chat.ddd.messages
import com.example.core_database.entities.ChatMessage
import com.example.core_database.entities.Bot
import com.example.core_database.entities.User
import com.example.core_database.DAO.ChatMessageDao
import io.getstream.chat.android.models.UserId
import kotlinx.coroutines.flow.Flow

class ChatMessageRepository(private val chatMessageDao:ChatMessageDao,
                            private val botId:String){
  val ChatMessages: Flow<List<ChatMessage>> =chatMessageDao.getMessagesForUserAndBot(botId)

  fun insert(chatMessage: ChatMessage){
    chatMessageDao.insert(chatMessage)
  }

  fun delete(chatMessage: ChatMessage){
    chatMessageDao.deleteMessage(chatMessage)
  }


}