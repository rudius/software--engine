package com.skydoves.chatgpt.feature.chat.ddd.bots

import com.example.core_database.entities.User
import com.example.core_database.entities.Bot
import com.example.core_database.entities.ChatMessage
import com.example.core_database.database.AppDatabase
import com.example.core_database.DAO.BotDao
import com.example.core_database.DAO.UserDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class BotRepository (
  private val botDao: BotDao,
) {
  val allBots: Flow<List<Bot>> = botDao.getAllBots()




  fun insert(bot: Bot) {
    botDao.insert(bot)
  }

  fun delete(bot: Bot) {
    botDao.deleteBot(bot)
  }



  fun updateNewMessage(botId:String,newMessage: ChatMessage){
    val current_bot=botDao.getBotByBotId(botId)
    current_bot.lastMessageTime=newMessage.timestamp
    current_bot.lastMessageContent=newMessage.messageContent
    botDao.updateBot(current_bot)
  }
}
