package com.skydoves.chatgpt.feature.chat.ddd.Channel3

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import com.example.core_database.entities.Bot
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens
import com.skydoves.chatgpt.feature.chat.ddd.bots.BotsViewModel
import com.skydoves.chatgpt.feature.chat.ddd.bots.CreateBotViewModel
import com.skydoves.chatgpt.feature.chat.ddd.bots.UiState

@Composable
fun ChatGPTMiddleBar3(
  modifier: Modifier,
  composeNavigator: AppComposeNavigator,
  viewModel: BotsViewModel = hiltViewModel(),
  createBotViewModel: CreateBotViewModel= hiltViewModel()
) {
  var username="null"
  createBotViewModel.getRealUid {s->
    username=s
  }

  val uiState by createBotViewModel.createState.collectAsStateWithLifecycle()

  var botName by remember { mutableStateOf("") }
  var description by remember { mutableStateOf("") }
  var prompt by remember { mutableStateOf("") }
  val image = remember { listOf(
    "https://gitee.com/rudius/software--engine/raw/master/Image/p1.jpg",
    "https://gitee.com/rudius/software--engine/raw/master/Image/p2.jpg",
    "https://gitee.com/rudius/software--engine/raw/master/Image/p3.jpg",
    "https://gitee.com/rudius/software--engine/raw/master/Image/p4.jpg",
  ) }

  var currentImageIndex by remember { mutableStateOf(0) }

  val rainbowColorsBrush = remember {
    Brush.sweepGradient(
      listOf(
        Color(0xFF9575CD),
        Color(0xFFBA68C8),
        Color(0xFFE57373),
        Color(0xFFFFB74D),
        Color(0xFFFFF176),
        Color(0xFFAED581),
        Color(0xFF4DD0E1),
        Color(0xFF9575CD)
      )
    )
  }
  val borderWidth = 4.dp

  Box{
    Column(
      modifier = modifier
        .statusBarsPadding()
        .verticalScroll(rememberScrollState()),
      horizontalAlignment = Alignment.CenterHorizontally,
      verticalArrangement = Arrangement.Center
    ) {
      Spacer(modifier = Modifier.height(20.dp))
      Box(
        modifier = Modifier
          .size(150.dp)
          .padding(8.dp)
          .clip(CircleShape)
      ){
        AsyncImage(
          model = image[currentImageIndex],
          contentDescription = null,
          contentScale = ContentScale.Crop,
          modifier = Modifier
            .size(150.dp)
            .border(
              BorderStroke(borderWidth, rainbowColorsBrush),
              CircleShape
            )
            .padding(borderWidth)
            .clip(CircleShape)
            .clickable {
              currentImageIndex = (currentImageIndex + 1) % image.size
            }
        )
      }

      OutlinedTextField(
        value = botName,
        onValueChange = { botName = it },
        label = { Text("名称") },
        maxLines = 1,
        shape = RoundedCornerShape(20.dp,20.dp,20.dp,20.dp),
        modifier = Modifier
          .padding(top = 20.dp)
          .padding(bottom = 20.dp)
          .padding(horizontal = 20.dp)
          .fillMaxWidth()
      )

      OutlinedTextField(
        value = description,
        onValueChange = { description = it },
        label = { Text("描述") },
        maxLines = 3,
        shape = RoundedCornerShape(20.dp,20.dp,20.dp,20.dp),
        modifier = Modifier
          .padding(bottom = 20.dp)
          .padding(horizontal = 20.dp)
          .height(100.dp)
          .fillMaxWidth()
      )

      OutlinedTextField(
        value = prompt,
        onValueChange = { prompt = it },
        label = { Text("人设") },
        maxLines = 3,
        shape = RoundedCornerShape(20.dp,20.dp,20.dp,20.dp),
        modifier = Modifier
          .padding(horizontal = 20.dp)
          .height(150.dp)
          .fillMaxWidth()
      )

      Button(
        onClick = {
          if(uiState==UiState.Nothing){
            createBotViewModel.createBot(username,botName,description,prompt){ response->
              val newBot = Bot(
                botId = response.botId,
                botName = botName,
                userId = username,
                conversationId = response.conversation_id,
                description = description,
                settings = "空",
                head = image[currentImageIndex],
                lastMessageTime = System.currentTimeMillis(),
                lastMessageContent = "hello,this is${botName}"
              )
              viewModel.insert(newBot)
              composeNavigator.navigate(ChatGPTScreens.Channel1.name)
            }
          }
        },
        colors = ButtonDefaults.buttonColors(
          containerColor = Color(0xFF2196F3), // 设置按钮背景颜色
          contentColor = Color.White // 设置文字颜色
        ),
        modifier = Modifier
          .padding(horizontal = 20.dp)
          .padding(top = 100.dp)
          .fillMaxWidth()
      ) {
        Text("创建Bot")
      }
    }

    if(uiState== UiState.Loading){
      CircularProgressIndicator(
        modifier = Modifier.align(Alignment.Center),
      )
    }

  }

}