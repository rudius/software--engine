package com.ddd.coze.feature.userlogin

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavOptionsBuilder
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens

@Composable
fun UserLogin(
  viewModel: UserLoginViewModel = hiltViewModel(),
  composeNavigator: AppComposeNavigator
) {
  val uiState by viewModel.loginState.collectAsStateWithLifecycle()

  var username by remember { mutableStateOf("") }
  var userPassword by remember { mutableStateOf("") }

  Box{
    Column(
      modifier = Modifier
        .statusBarsPadding()
        .padding(vertical = 40.dp, horizontal = 40.dp),
      horizontalAlignment = Alignment.CenterHorizontally,
      verticalArrangement = Arrangement.Center
    ) {
      Text(
        text = "Log In",
        fontSize = 24.sp,    // 设置字体大小
        fontWeight = FontWeight.Bold,    // 设置字体为粗体
        modifier = Modifier
          .padding(top = 16.dp, bottom = 16.dp) // 设置底部内边距
          .align(Alignment.CenterHorizontally) // 水平居中
      )
      EditStringField(
        label = "用户名",
        keyboardOptions = KeyboardOptions.Default,
        value = username,
        onValueChange = { username = it },
        modifier = Modifier
          .padding(bottom = 32.dp)
          .fillMaxWidth()
      )
      EditStringField(
        label = "password",
        keyboardOptions = KeyboardOptions.Default.copy(
          keyboardType = KeyboardType.Password
        ),
        value = userPassword,
        onValueChange = { userPassword = it },
        modifier = Modifier
          .padding(bottom = 32.dp)
          .fillMaxWidth()
      )
      Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween // 设置按钮之间的间距
      ) {
        // 登录按钮
        Button(
          onClick = {
            if(uiState == UiState.Nothing){
              viewModel.login(username, userPassword) {
                composeNavigator.navigate(ChatGPTScreens.Channel1.name)
              }
            }

          },
          colors = ButtonDefaults.buttonColors(
            containerColor = Color(0xFF2196F3), // 设置按钮背景颜色
            contentColor = Color.White // 设置文字颜色
          ),
          modifier = Modifier
            .weight(1f) // 设置按钮宽度相等
            .padding(end = 16.dp) // 右侧内边距
        ) {
          Text("登录")
        }
        // 注册按钮
        Button(
          onClick = {
            composeNavigator.navigate(ChatGPTScreens.UserSignin.route)
          },

          colors = ButtonDefaults.buttonColors(
            containerColor = Color.LightGray, // 设置按钮背景颜色
            contentColor = Color.Black // 设置文字颜色
          ),
          modifier = Modifier
            .weight(1f) // 设置按钮宽度相等
            .padding(start = 16.dp) // 左侧内边距
        ) {
          Text("注册")
        }
      }

      Spacer(modifier = Modifier.height(200.dp)) // 调整这个值来控制偏移量


    }

    if(uiState==UiState.Loading){
      CircularProgressIndicator(
        modifier = Modifier.align(Alignment.Center),
      )
    }




  }



}


@Composable
fun EditStringField(//输入框组件
  //@StringRes label: Int,
  label: String,
  keyboardOptions: KeyboardOptions,
  value: String,
  onValueChange: (String) -> Unit,
  modifier: Modifier = Modifier
) {

  TextField(
    value = value,
    onValueChange = onValueChange,
    singleLine = true,
    label = { Text(label) },
    keyboardOptions = keyboardOptions,
    modifier = modifier
  )
}




@Preview(showBackground = true)
@Composable
fun PreviewUserLogin() {
  // 提供一个完整的 mock AppComposeNavigator 实现
  val mockComposeNavigator = object : AppComposeNavigator() {
    override fun navigate(route: String, optionsBuilder: (NavOptionsBuilder.() -> Unit)?) {
      // 空实现，不做任何操作
    }
    override fun <T> navigateBackWithResult(key: String, result: T, route: String?) {}
    override fun popUpTo(route: String, inclusive: Boolean) {}
    override fun navigateAndClearBackStack(route: String) {}
  }


  UserLogin(composeNavigator = mockComposeNavigator)

}


