package com.ddd.coze.feature.usersignin

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.skydoves.chatgpt.core.data.repository.AppUserRepository
import com.skydoves.chatgpt.core.model.network.SigninResponse

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserSignViewModel @Inject constructor(
  private val userRepository: AppUserRepository
) : ViewModel() {

  var errorMessage: String? = null
    private set

  fun signin(username: String, password: String, onSuccess: (SigninResponse) -> Unit) {
    try {
      viewModelScope.launch {
        val response = userRepository.signin(username, password)
        if (response != null) {
          onSuccess(response)
        } else {
          errorMessage = "注册失败"
        }
      }
    }catch (e:Exception){
      println("Error: ${e.localizedMessage}")
    }

  }
}