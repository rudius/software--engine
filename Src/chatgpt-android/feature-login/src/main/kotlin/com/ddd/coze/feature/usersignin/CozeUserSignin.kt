package com.ddd.coze.feature.usersignin

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavOptionsBuilder
import com.ddd.coze.feature.userlogin.UserLoginViewModel
import com.skydoves.chatgpt.core.designsystem.theme.ChatGPTComposeTheme
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens

@Composable
fun UserSignin(
  composeNavigator: AppComposeNavigator
) {
  val viewModel: UserSignViewModel = hiltViewModel()

  var username by remember { mutableStateOf("") }
  var userPassword by remember { mutableStateOf("") }
  var userPassword_repeat by remember { mutableStateOf("") }
  var isPasswordMatch by remember { mutableStateOf(true) }

  Column(
    modifier = Modifier
      .statusBarsPadding()
      .padding(vertical = 40.dp, horizontal = 40.dp),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center
  ) {
    Text(
      text = "Sign In",
      fontSize = 24.sp,    // 设置字体大小
      fontWeight = FontWeight.Bold,    // 设置字体为粗体
      modifier = Modifier
        .padding(top = 16.dp, bottom = 16.dp) // 设置底部内边距
        .align(Alignment.CenterHorizontally) // 水平居中
    )
    EditStringField(
      label = "用户名",
      keyboardOptions = KeyboardOptions.Default,
      value = username,
      onValueChange = { username = it },
      modifier = Modifier
        .padding(bottom = 32.dp)
        .fillMaxWidth()
    )
    EditStringField(
      label = "password",
      keyboardOptions = KeyboardOptions.Default.copy(
        keyboardType = KeyboardType.Password
      ),
      value = userPassword,
      onValueChange = { userPassword = it },
      modifier = Modifier
        .padding(bottom = 32.dp)
        .fillMaxWidth()
    )
    EditStringField(
      label = "repeat password",
      keyboardOptions = KeyboardOptions.Default.copy(
        keyboardType = KeyboardType.Password
      ),
      value = userPassword_repeat,
      onValueChange = {
        userPassword_repeat = it
        isPasswordMatch = (userPassword == it)
      },
      modifier = Modifier
        .padding(bottom = 32.dp)
        .fillMaxWidth()
    )

    if (!isPasswordMatch) {
      Text(
        text = "密码不匹配，请重试",
        color = Color.Red,
        modifier = Modifier.padding(bottom = 16.dp)
      )
    }

    Spacer(modifier = Modifier.height(20.dp))

    Row(
      modifier = Modifier.fillMaxWidth(),
      horizontalArrangement = Arrangement.SpaceBetween // 设置按钮之间的间距
    ) {

      Button(
        onClick = {
          if (isPasswordMatch) {

            viewModel.signin(username, userPassword) { response ->
                composeNavigator.navigate(ChatGPTScreens.UserLogin.route)
            }
          }
        },
        colors = ButtonDefaults.buttonColors(
          containerColor = Color(0xFF2196F3), // 设置按钮背景颜色
          contentColor = Color.White // 设置文字颜色
        ),
        modifier = Modifier
          .weight(1f) // 设置按钮宽度相等
          .padding(end = 16.dp) // 右侧内边距
      ) {
        Text("提交注册")
      }

      Button(
        onClick = { composeNavigator.navigate(ChatGPTScreens.UserLogin.route) },
        colors = ButtonDefaults.buttonColors(
          containerColor = Color.LightGray, // 设置按钮背景颜色
          contentColor = Color.Black // 设置文字颜色
        ),
        modifier = Modifier
          .weight(1f) // 设置按钮宽度相等
          .padding(start = 16.dp) // 左侧内边距
      ) {
        Text("返回登录")
      }
    }

    Spacer(modifier = Modifier.height(150.dp)) // 调整这个值来控制偏移量



  }
}


@Composable
fun EditStringField(//输入框组件
  //@StringRes label: Int,
  label: String,
  keyboardOptions: KeyboardOptions,
  value: String,
  onValueChange: (String) -> Unit,
  modifier: Modifier = Modifier
) {

  TextField(
    value = value,
    onValueChange = onValueChange,
    singleLine = true,
    label = { Text(label) },
    keyboardOptions = keyboardOptions,
    modifier = modifier
  )
}




@Preview(showBackground = true)
@Composable
fun PreviewUserLogin() {
  // 提供一个完整的 mock AppComposeNavigator 实现
  val mockComposeNavigator = object : AppComposeNavigator() {
    override fun navigate(route: String, optionsBuilder: (NavOptionsBuilder.() -> Unit)?) {
      // 空实现，不做任何操作
    }
    override fun <T> navigateBackWithResult(key: String, result: T, route: String?) {}
    override fun popUpTo(route: String, inclusive: Boolean) {}
    override fun navigateAndClearBackStack(route: String) {}
  }

  ChatGPTComposeTheme {
    UserSignin(composeNavigator = mockComposeNavigator)
  }
}


