/*
 * Designed and developed by 2024 skydoves (Jaewoong Eum)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.chatgpt.core.designsystem.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.skydoves.chatgpt.core.navigation.ChatGPTScreens
import com.skydoves.chatgpt.core.navigation.AppComposeNavigator

@Composable
fun ChatGPTSmallTopBar(title: String) {
  TopAppBar(
    modifier = Modifier.fillMaxWidth(),
    title = {
      Text(
        text = title,
        color = MaterialTheme.colorScheme.tertiary,
        style = MaterialTheme.typography.titleLarge
      )
    },
    colors = TopAppBarDefaults.mediumTopAppBarColors(
      containerColor = Color(0xFF5626C7)
    )
  )
}

@Composable
fun ChatGPTSmallBottomBar1(
  composeNavigator: AppComposeNavigator
) {
  BottomAppBar(
    modifier = Modifier.fillMaxWidth(),
    actions = {
      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {},) {
          Icon(
            Icons.Filled.Home,
            contentDescription = "主页",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "主页",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel2.name)}) {
          Icon(
            Icons.Filled.Search,
            contentDescription = "发现",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "发现",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel3.name)}) {
          Icon(
            Icons.Filled.Create,
            contentDescription = "创建",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "创建",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel4.name)}) {
          Icon(
            Icons.Filled.Person,
            contentDescription = "我",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "我",
        )
      }
    }
  )
}

@Composable
fun ChatGPTSmallBottomBar2(
  composeNavigator: AppComposeNavigator
) {
  BottomAppBar(
    modifier = Modifier.fillMaxWidth(),
    actions = {
      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel1.name)},) {
          Icon(
            Icons.Filled.Home,
            contentDescription = "主页",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "主页",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {}) {
          Icon(
            Icons.Filled.Search,
            contentDescription = "发现",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "发现",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel3.name)}) {
          Icon(
            Icons.Filled.Create,
            contentDescription = "创建",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "创建",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel4.name)}) {
          Icon(
            Icons.Filled.Person,
            contentDescription = "我",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "我",
        )
      }
    }
  )
}

@Composable
fun ChatGPTSmallBottomBar3(
  composeNavigator: AppComposeNavigator
) {
  BottomAppBar(
    modifier = Modifier.fillMaxWidth(),
    actions = {
      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel1.name)},) {
          Icon(
            Icons.Filled.Home,
            contentDescription = "主页",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "主页",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel2.name)}) {
          Icon(
            Icons.Filled.Search,
            contentDescription = "发现",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "发现",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {}) {
          Icon(
            Icons.Filled.Create,
            contentDescription = "创建",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "创建",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel4.name)}) {
          Icon(
            Icons.Filled.Person,
            contentDescription = "我",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "我",
        )
      }
    }
  )
}

@Composable
fun ChatGPTSmallBottomBar4(
  composeNavigator: AppComposeNavigator
) {
  BottomAppBar(
    modifier = Modifier.fillMaxWidth(),
    actions = {
      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel1.name)},) {
          Icon(
            Icons.Filled.Home,
            contentDescription = "主页",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "主页",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel2.name)}) {
          Icon(
            Icons.Filled.Search,
            contentDescription = "发现",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "发现",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {composeNavigator.navigate(ChatGPTScreens.Channel3.name)}) {
          Icon(
            Icons.Filled.Create,
            contentDescription = "创建",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "创建",
        )
      }

      Column(
        Modifier.weight(1f),
        horizontalAlignment = Alignment.CenterHorizontally
      ) {
        IconButton(onClick = {}) {
          Icon(
            Icons.Filled.Person,
            contentDescription = "我",
            Modifier.weight(1f)
          )
        }
        Text(
          text = "我",
        )
      }
    }
  )
}


//@Composable
//fun ChatGPTMiddleBar4(
//  modifier: Modifier
//) {
//  val image = remember { listOf(
//    "https://img.picui.cn/free/2024/11/11/6731ba0341c4f.jpg",
//    "https://img.picui.cn/free/2024/11/11/6731bce36b858.png",
//    "https://img.picui.cn/free/2024/11/11/6731bce61256d.png",
//    "https://img.picui.cn/free/2024/11/11/6731bce621917.png"
//  ) }
//  var currentIndex by remember { mutableStateOf(0) }
//  LaunchedEffect(Unit) {
//    while (true) {
//      delay(5000) // 设置每隔2秒切换一次图片
//      currentIndex = (currentIndex + 1) % image.size
//    }
//  }
//
//  Column(
//    modifier = modifier
//      .statusBarsPadding(),
//    horizontalAlignment = Alignment.CenterHorizontally,
//    verticalArrangement = Arrangement.Center
//  ) {
//    Box(
//      modifier = Modifier
//        .height(400.dp)
//        .fillMaxWidth()
//    ){
//      Crossfade(
//        targetState = image[currentIndex],
//        animationSpec = tween(durationMillis = 500, delayMillis = 0, easing = FastOutSlowInEasing),
//        label = ""
//      ) { targetImage ->
//        AsyncImage(
//          model = targetImage,
//          contentDescription = null, // 可以为null
//          contentScale = ContentScale.FillBounds, // 填充整个Box
//          modifier = Modifier.fillMaxSize()
//        )
//      }
//      Box(
//        modifier = Modifier
//          .padding(start = 20.dp)
//          .padding(top = 30.dp)
//          .size(120.dp)
//          .padding(8.dp)
//          .clip(CircleShape)
//      ) {
//        AsyncImage(
//          model = "https://user-images.githubusercontent.com/24237865/206655413-fb7c70f6-703e-476b-9ee9-861bfb8bf6f7.jpeg",
//          contentDescription = null,
//          contentScale = ContentScale.Crop,
//          modifier = Modifier
//            .border(4.dp, Color.Black, CircleShape)
//            .size(120.dp)
//            .clip(CircleShape)
//            .clickable {
//
//            }
//        )
//      }
//    }
//  }
//}


