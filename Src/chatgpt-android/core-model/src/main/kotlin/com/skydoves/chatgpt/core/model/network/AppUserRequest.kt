package com.skydoves.chatgpt.core.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class CreateBotRequest(
  @field:Json(name = "botName") val botName: String,
  @field:Json(name = "description") val description: String,
  @field:Json(name = "promptInfo") val promptInfo: String
)

