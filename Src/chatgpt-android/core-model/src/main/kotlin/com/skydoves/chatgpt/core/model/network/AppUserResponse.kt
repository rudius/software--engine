package com.skydoves.chatgpt.core.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginResponse(
  @field:Json(name = "message") val message: String,
  @field:Json(name = "status") val status: String,
  //@field:Json(name = "uid") val uid: String,
)

@JsonClass(generateAdapter = true)
data class SigninResponse(
  @field:Json(name = "message") val message: String,
  @field:Json(name = "status") val status: String,
)

//一次聊天的返回
@JsonClass(generateAdapter = true)
data class ChatResponse(
  @field:Json(name = "data") val data: String,
  @field:Json(name = "status") val status: String,
)

//创建智能体返回
@JsonClass(generateAdapter = true)
data class CreateBotResponse(
  @field:Json(name = "status") val status: String,
  @field:Json(name = "bot_id") val botId: String,
  @field:Json(name = "conversation_id") val conversation_id: String,
)