/*
 * Designed and developed by 2024 skydoves (Jaewoong Eum)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.chatgpt.core.network.di

import com.skydoves.chatgpt.core.network.BuildConfig
import com.skydoves.chatgpt.core.network.GPTInterceptor
import com.skydoves.chatgpt.core.network.service.ChatGPTService
import com.skydoves.chatgpt.core.network.service.UserService
import com.skydoves.sandwich.retrofit.adapters.ApiResponseCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ChatGPTRetrofit

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class UserRetrofit

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ChatGPTClient

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class UserClient

@Module
@InstallIn(SingletonComponent::class)
internal object NetworkModule {

  @Provides
  @Singleton
  @ChatGPTClient
  fun provideChatGPTOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
      .addInterceptor(GPTInterceptor())
      .connectTimeout(60, TimeUnit.SECONDS)
      .readTimeout(60, TimeUnit.SECONDS)
      .writeTimeout(15, TimeUnit.SECONDS)
      .apply {
        if (BuildConfig.DEBUG) {
          this.addNetworkInterceptor(
            HttpLoggingInterceptor().apply {
              level = HttpLoggingInterceptor.Level.BODY
            }
          )
        }
      }
      .build()
  }

  @Provides
  @Singleton
  @UserClient
  fun provideUserOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
      //.addInterceptor(RetryInterceptor())
      .connectTimeout(60, TimeUnit.SECONDS)
      .readTimeout(60, TimeUnit.SECONDS)
      .writeTimeout(15, TimeUnit.SECONDS)
      .apply {
        if (BuildConfig.DEBUG) {
          this.addNetworkInterceptor(
            HttpLoggingInterceptor().apply {
              level = HttpLoggingInterceptor.Level.BODY
            }
          )
        }
      }
      .build()
  }



  @Provides
  @Singleton
  @ChatGPTRetrofit
  fun provideChatGPTRetrofit(@ChatGPTClient okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
      .client(okHttpClient)
      .baseUrl("https://api.openai.com/")
      .addConverterFactory(MoshiConverterFactory.create())
      .addCallAdapterFactory(ApiResponseCallAdapterFactory.create())
      .build()
  }



  @Provides
  @Singleton
  fun provideChatGPTService(@ChatGPTRetrofit retrofit: Retrofit): ChatGPTService = retrofit.create(ChatGPTService::class.java)


  @Provides
  @Singleton
  @UserRetrofit
  fun provideUserRetrofit(@UserClient okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
      .client(okHttpClient)
      .baseUrl("http://1.94.239.166:8086/")
      .addConverterFactory(MoshiConverterFactory.create())
      .addCallAdapterFactory(ApiResponseCallAdapterFactory.create())
      .build()
  }


  @Provides
  @Singleton
  fun provideUserService(@UserRetrofit userRetrofit: Retrofit): UserService = userRetrofit.create(UserService::class.java)

}


class RetryInterceptor(private val maxRetryCount: Int = 3) : Interceptor {

  private var retryCount = 0

  override fun intercept(chain: Interceptor.Chain): Response {
    var response: Response? = null
    var lastException: Exception? = null

    while (retryCount < maxRetryCount) {
      try {
        response = chain.proceed(chain.request())
        // 如果请求成功，则返回响应
        if (response.isSuccessful) {
          return response
        }
      } catch (e: Exception) {
        lastException = e
        retryCount++
        println("Request failed, retrying... ($retryCount/$maxRetryCount)")
      }
    }

    // 如果超过最大重试次数仍然失败，则抛出最后一个异常
    throw lastException ?: IllegalStateException("Unknown error occurred.")
  }
}