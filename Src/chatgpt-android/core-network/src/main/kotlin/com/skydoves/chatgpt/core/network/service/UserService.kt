package com.skydoves.chatgpt.core.network.service

import com.skydoves.chatgpt.core.model.network.ChatResponse
import com.skydoves.chatgpt.core.model.network.CreateBotResponse
import com.skydoves.chatgpt.core.model.network.LoginResponse
import com.skydoves.chatgpt.core.model.network.SigninResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UserService {
  @FormUrlEncoded
  @POST("/login")
  suspend fun login(
    @Field("username") username: String,
    @Field("password") password: String
  ): Response<LoginResponse>

  @FormUrlEncoded
  @POST("/register")
  suspend fun signin(
    @Field("username") username: String,
    @Field("password") password: String
  ): Response<SigninResponse>

  //发起一次对话
  @FormUrlEncoded
  @POST("/chat")
  suspend fun chat(
    @Field("username") username: String,
    @Field("bot_id") bot_id: String,
    @Field("question") question: String,
    @Field("conversation_id") conversation_id: String,
  ): Response<ChatResponse>

  @FormUrlEncoded
  @POST("/create_bot")
  suspend fun createBot(
    @Field("username") username: String,
    @Field("bot_name") botName: String,
    @Field("bot_description") botDescription: String,
    @Field("prompt") prompt: String,
  ): Response<CreateBotResponse>

//  @POST("/create_bot")
//  suspend fun createBot(@Body request: CreateBotRequest): Response<CreateBotResponse>




}