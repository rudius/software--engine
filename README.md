# 如聊 Like-chat

------



## 项目介绍

华中科技大学计算机科学与技术学院软件工程课程的小组作业，小组成员历时一个半月从零开始学习安卓开发的成果。

这是一款Android App，是前后端完全分离，严格按照安卓软件设计标注架构开发的app。整个app是在Android Studio中开发的，100%Kotlin。服务端我们已经部署了云服务器，为app提供一系列联网服务，后端使用python开发。

### 软件功能
如聊app，通过调用扣子专业版API，提供创建智能体，与人工智能进行聊天对话。同时用户也可以选择预制好的不同智能体进行对话。相较于普通的ai，如聊注重用户聊天记录的存储，使得智能体的记忆长效，难以忘记用户的个人偏好。随着聊天的时间延长，智能体的性格会越发逼近用户设置的，使用户体验感更加逼真，聊天更加沉浸。如聊通过隐式发起对话，模拟出智能体主动找用户聊天的情况，避免了单一的问答式对话，增强用户于智能体的交流体验。

### 架构设计概述

#### 设计理念

***分层设计：***

如聊app严格按照Android开发者文档中的推荐架构原则设计：每个应用应该包含界面层、数据层、网域层。

<img src="Image/mad-arch-overview.png" style="zoom: 25%;" />

***模块化：***

包含多个 Gradle 模块的项目称为多模块项目。模块化是按多个松散耦合的独立部分整理代码库的做法。每个部分都是一个模块。每个模块都是独立的，并且都有明确的用途。通过将问题划分为更小、更易于解决的子问题，降低设计和维护大型系统的复杂性。

表征模块化代码库的一种方式是使用**耦合**和**内聚**属性。

- **低耦合**是指模块应尽可能相互独立，这样一来，对一个模块所做的更改将对其他模块产生零影响或极小的影响。**模块相互之间不应了解对方的内部运行原理**。
- **高内聚**是指多个模块的代码集合应当像一个系统一样运行。它们应具有明确定义的职责，并始终位于特定领域知识范围以内。假设有一个电子书应用示例。在同一个模块中融合图书相关代码和付款相关代码是不合适的，因为图书和付款是两个不同的功能领域。



#### 如聊模块化列表

| 序号 | 模块名称          | 模块作用                                                     |
| --------- | ----------------- |----------------------------------------- |
| 1    | core-data | 定义了repository，连接界面层和网络模块。定义AppModule，提供全局方法。 |
| 2    | core-database     | 本地Room数据库相关模块，定义数据结构和一系列数据库方法。     |
| 3    | core-designsystem | 界面上部，下部通用UI组件                                     |
| 4    | core-model        | 定义了与服务器通信的Request，Response格式                    |
| 5    | core-navigation   | 导航组件相关代码                                             |
| 6    | core-network      | 为应用提供网络服务支持，定义了用户相关的网络请求接口，如登录、注册、聊天和其他用户操作的 API 方法，用于在应用内与后端服务器通信。负责提供网络依赖项的配置和注入。创建和提供 `Retrofit`、`OkHttpClient` 等网络库实例，确保网络请求的有效管理和统一配置。 |
| 7    | feature-login     | 登录，注册界面相关的UI组件，ViewModel                        |
| 8    | feature-chat      | 主页，发现，创建智能体，个人展示，聊天界面相关UI组件，ViewModel |

#### 系统结构

app的前端界面层全部使用 [Jetpack Compose](https://developer.android.google.cn/jetpack/compose?hl=zh-cn)组件完成，通过ViewModel与数据库交互。后端包括本地Room数据库和部署在云端的服务器。整个app实现了前后端分离开发。下图为系统结构示意图。

![](Image/系统结构.png)



------



## 项目展示

- ***登录和注册界面***

<img src="Image/1.png" style="zoom: 20%;" />

- ***主体界面***

| 主页                                         | 发现                                         | 创建                                         | 我的                                         |
| -------------------------------------------- | -------------------------------------------- | -------------------------------------------- | -------------------------------------------- |
| <img src="Image/c1.png" style="zoom:25%;" /> | <img src="Image/c2.png" style="zoom:25%;" /> | <img src="Image/c3.png" style="zoom:25%;" /> | <img src="Image/c4.png" style="zoom:25%;" /> |

- ***聊天界面***（部分案例展示）

| 奶龙                                               | 口算大师                                           | 消息弹窗                                            |
| -------------------------------------------------- | -------------------------------------------------- | --------------------------------------------------- |
| <img src="Image/message1.png" style="zoom:25%;" /> | <img src="Image/message2.png" style="zoom:25%;" /> | <img src="Image/message3.jpg" style="zoom: 33%;" /> |

## 文件组织结构

- **Doc**：存放项目相关文档
-  **Src**：存放项目代码
  - chatgpt-android ：Android Studio项目文件夹
- **Image**：存放项目相关图片
- **Apk**：存放可以直接安装的 .apk 文件

## 项目成员与分工

| 成员   | 分工                                                         | 贡献 |
| ----------- | ------------------------------------------- | ---- |
| 冼锡亮      | 云服务器部署；云服务器上前端与cozeAPI的中转逻辑实现；云端数据库的设计与实现 | 25%  |
| 徐一鸣 | 后端数据库的设计与实现；部分viewmodel的逻辑实现；部分UI界面函数的逻辑实现       | 25%  |
| 丁丁丁 | 前端界面ViewModel逻辑实现；收发消息，网络层与云服务器通信接口设计，代码实现； | 25%  |
| 王靖宇 | 前端所有UI界面搭建；后期UI美化，调试界面，体验优化； | 25%  |

## 项目安装

- 下载新版[Android Studio](https://developer.android.google.cn/studio?hl=zh-cn)
- 用Android Studio打开Scr文件夹中的chatgpt-android，等待构建完成即可。

## 更多详细文档见Doc文件夹或者Wiki

1. [**NABCD**](https://gitee.com/rudius/software--engine/wikis/NABCD)
2. [**需求分析**](https://gitee.com/rudius/software--engine/wikis/%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90)
3. [**原型系统**](https://gitee.com/rudius/software--engine/wikis/%E5%8E%9F%E5%9E%8B%E7%B3%BB%E7%BB%9F)
4. [**进度计划**](https://gitee.com/rudius/software--engine/wikis/%E8%BF%9B%E5%BA%A6%E8%AE%A1%E5%88%92)
5. [**系统设计**](https://gitee.com/rudius/software--engine/wikis/%E7%B3%BB%E7%BB%9F%E8%AE%BE%E8%AE%A1)
6. [**用户反馈**](https://gitee.com/rudius/software--engine/wikis/%E7%94%A8%E6%88%B7%E5%8F%8D%E9%A6%88)
7. **[实现与测试](https://gitee.com/rudius/software--engine/wikis/实现与测试)**

